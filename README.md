# test-cpp-course

Репозиторий для разработки / тестирования инфраструктуры:

- [Clippy](https://gitlab.com/Lipovsky/clippy)
- [Pequod](https://gitlab.com/Lipovsky/pequod)

## Инструкции

1) [Начало работы](docs/setup.md)
2) [Как сдавать задачи](docs/ci.md)

## Навигация

- [Задачи](/tasks)
- [Дедлайны](/deadlines)
- [Manytask](http://178.154.225.159:5111/signup)
- [Результаты](https://docs.google.com/spreadsheets/d/1Pu_fET2IWj-PblX29wGAyv_mI7c9OBhX_DVpHAf3u1c/edit?usp=sharing)